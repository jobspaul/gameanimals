//
//  Page1ViewController.swift
//  ProjectAnimal
//
//  Created by Nattawut Nokyoo on 3/24/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class Page1ViewController: UIViewController {
    @IBOutlet weak var showLabel: UILabel!
    @IBOutlet weak var buttonGoToPage2: UIButton!
    @IBOutlet weak var buttonGoToPage3: UIButton!
    @IBOutlet weak var buttonGoToPage4: UIButton!
    var sum2:Int = 0
    var sum3:String = ""

    func setViews() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViews()
        //FlowModel.sharedInstance.summarry += 1
        
        if FlowModel.sharedInstance.summarry == 0 {
            
            
            FlowModel.sharedInstance.summarry += 0
            //print("Point 1: \(FlowModel.sharedInstance.summarry)")// Do any additional setup after loading the view.
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
            
        }

    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //print("Point : \(FlowModel.sharedInstance.summarry)")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ////ปุ่มตัวเลือก
    @IBAction func buttonGoToPage2(_ sender: Any) {
        self.performSegue(withIdentifier: "goToPage2", sender: self)
        FlowModel.sharedInstance.summarry += 1
        sum2 = FlowModel.sharedInstance.summarry
        //print("Point : \(sum2)")
        sum3 = String(sum2)
        self.showLabel.text! = sum3
        
    }
    
    @IBAction func buttonGoToPage3(_ sender: Any) {
        self.performSegue(withIdentifier: "goToPage2", sender: self)
        FlowModel.sharedInstance.summarry += 0
        sum2 = FlowModel.sharedInstance.summarry
        //print("Point : \(sum2)")
        sum3 = String(sum2)
        self.showLabel.text! = sum3
    }
    
    @IBAction func buttonGoToPage4(_ sender: Any) {
        self.performSegue(withIdentifier: "goToPage2", sender: self)
        FlowModel.sharedInstance.summarry += 0
        sum2 = FlowModel.sharedInstance.summarry
        //print("Point : \(sum2)")
        sum3 = String(sum2)
        self.showLabel.text! = sum3
    }

    @IBAction func backButton(_ sender: UIButton) {
        FlowModel.sharedInstance.summarry *= 0
        //print("Point 1: \(FlowModel.sharedInstance.summarry)")
        _ = self.navigationController?.popViewController(animated: true)
        //FlowModel.sharedInstance.summarry -= 1
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
