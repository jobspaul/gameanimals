//
//  LastNumberViewController.swift
//  ProjectAnimal
//
//  Created by Nattawut Nokyoo on 3/25/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class LastNumberViewController: UIViewController {
    @IBOutlet weak var showLabel: UILabel!
    var sum2:Int = 0
    var sum3:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        if FlowModel.sharedInstance.summarry == 1 {
            
            
            FlowModel.sharedInstance.summarry += 0
            //print("Point 1: \(FlowModel.sharedInstance.summarry)")// Do any additional setup after loading the view.
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
            
        }
        else {
            FlowModel.sharedInstance.summarry += 0
            //print("Point 1: \(FlowModel.sharedInstance.summarry)")// Do any additional setup after loading the view.
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backMenu(_ sender: UIButton) {
        _ = self.navigationController?.popToRootViewController(animated: true)
        FlowModel.sharedInstance.summarry *= 0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
