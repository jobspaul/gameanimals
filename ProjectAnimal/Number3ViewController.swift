//
//  Number3ViewController.swift
//  ProjectAnimal
//
//  Created by Nattawut Nokyoo on 3/25/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class Number3ViewController: UIViewController {
    @IBOutlet weak var showLabel: UILabel!
    @IBOutlet weak var inputText: UITextField!
    @IBOutlet weak var buttonGoToPage2: UIButton!
    
    var sum2:Int = 0
    var sum3:String = ""
    var value:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if FlowModel.sharedInstance.summarry == 1 {
            
            
            FlowModel.sharedInstance.summarry += 0
            //print("Point 1: \(FlowModel.sharedInstance.summarry)")// Do any additional setup after loading the view.
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
            
        }
        else {
            FlowModel.sharedInstance.summarry += 0
            //print("Point 1: \(FlowModel.sharedInstance.summarry)")// Do any additional setup after loading the view.
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGoToPage2(_ sender: UIButton) {
        if (inputText.text?.isEmpty)! {
            self.performSegue(withIdentifier: "goToNumber4", sender: self)
            FlowModel.sharedInstance.summarry += 0
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
        }
        
        else if inputText.text == "4" {
            self.performSegue(withIdentifier: "goToNumber4", sender: self)
            FlowModel.sharedInstance.summarry += 1
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
        }
            
        else{
            self.performSegue(withIdentifier: "goToNumber4", sender: self)
            FlowModel.sharedInstance.summarry += 0
            sum2 = FlowModel.sharedInstance.summarry
            sum3 = String(sum2)
            self.showLabel.text! = sum3
        }

    }
    
    @IBAction func backMenu(_ sender: UIButton) {
        FlowModel.sharedInstance.summarry *= 0
        //print("Point 1: \(FlowModel.sharedInstance.summarry)")
        _ = self.navigationController?.popToRootViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
